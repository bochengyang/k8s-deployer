FROM alpine:3.6
MAINTAINER Bocheng Yang <bocheng.yang@gmail.com>

ENV KUBE_LATEST_VERSION="v1.10.3"
ENV HELM_LATEST_VERSION="v2.9.1"

ADD https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl /usr/local/bin/kubectl

RUN apk add --update ca-certificates && \
    apk add --update -t deps curl && \
    apk add --update gettext && \
    chmod +x /usr/local/bin/kubectl && \
    apk del --purge deps && \
    rm /var/cache/apk/* && \
    \
    # Basic check it works.
    kubectl version --client

ADD https://storage.googleapis.com/kubernetes-helm/helm-${HELM_LATEST_VERSION}-linux-amd64.tar.gz /

RUN cd / && \
    tar zxf helm-${HELM_LATEST_VERSION}-linux-amd64.tar.gz && \
    mv /linux-amd64/helm /usr/local/bin/helm && \
    rm -rf /linux-amd64 && \
    rm /helm-${HELM_LATEST_VERSION}-linux-amd64.tar.gz && \
    helm
