# k8s-deployer
The docker image that included kubectl for the deployment in kubernetes cluster

> Thanks to the [lwolf](https://github.com/lwolf/kubectl-deployer-docker) and [wernight](https://github.com/wernight/docker-kubectl) for the idea

|Tag       | Alpine |   Kubectl    |
|:--------:|:------:|:------------:|
|latest    |3.6     |1.7.3         |
|0.1       |3.6     |1.7.3         |

# How to use this container
This container is a tool with kubectl command that help you to access your kubernetes cluster, especially can be used in GitLab™ CI.

For how to use this container in GitLab™, you first need to create .gitlab-ci.yml (default) to enable CI feature. Then, you can write your own test and build process. 

You will need another yaml file for deploying your deployment to kubernetes, it can refer to [here](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) for the detail of yaml file.

The last stage of GitLab™ CI is deploy, you can then use this container and the kubernetes deployment yaml file to run your application. You can reference the following example which is the section of deploy.

```yaml
deploy-develop:
  stage: deploy
  image: bcyang/k8s-deployer:latest
  services:
    - docker:dind
  script:
    - echo "I'm development or feature, token $K8S_TOKEN"
    - kubectl config set-cluster my-cluster --server="https://[host]:[port]" --insecure-skip-tls-verify=true
    - kubectl config set-credentials admin --token="$K8S_TOKEN"
    - kubectl config set-context default-context --cluster=my-cluster --user=admin
    - kubectl config use-context default-context
    - kubectl get cs
```

> The variable $K8S_TOKEN is from your kubernetes cluster, the following command help you to to get the token from your kubernetes cluster (Please execute this command on your master node). Also, you must set the variable into Setting -> Pipelines -> Secret variables
> ```bash
> kubectl describe secret $(kubectl get secrets | grep default | cut -f1 -d ' ') | grep -E '^token' | cut -f2 -d':' | tr -d '\t'
> ```